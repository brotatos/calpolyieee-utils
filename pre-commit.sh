#!/bin/sh

JS_DIR="$GIT_DIR/../js"
CSS_DIR="$GIT_DIR/../css"

JS_COMBINED_FILE="$JS_DIR/calpolyieee-utils.min.js"
CSS_COMBINED_FILE="$CSS_DIR/calpolyieee-utils.min.css"

JS_FILES=( calpolyieee-utils.js Form.js Form.gen.js ModalAlert.js )
CSS_FILES=( Form.css )

# --- Don't edit below here ---
CSS_TEMP="$CSS_DIR/_combined.css"
JS_TEMP="$JS_DIR/_combined.js"

> $CSS_COMBINED_FILE
> $JS_COMBINED_FILE

for F in ${JS_FILES[@]}; do
    cat "$JS_DIR/$F" >> $JS_TEMP
done

for F in ${CSS_FILES[@]}; do
    cat "$CSS_DIR/$F" >> $CSS_TEMP
done

java -jar /Users/jhladky/Documents/yuicompressor.jar -o $JS_COMBINED_FILE $JS_TEMP
java -jar /Users/jhladky/Documents/yuicompressor.jar -o $CSS_COMBINED_FILE $CSS_TEMP

rm $JS_TEMP
rm $CSS_TEMP

git add $JS_COMBINED_FILE
git add $CSS_COMBINED_FILE
