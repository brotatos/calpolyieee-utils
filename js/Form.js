"use strict";

utils.Form = function (skel, config) {
    var self = this;

    this.$container = $("<div class=\"utilsForm\">");
    this.$messages = $("<div>");
    this.$form = $("<div class=\"form-horizontal\">");
    this.$title = utils.Form.gen.p("&nbsp;", "<h4>" + config.title + "</h4>");
    this.$confirm = utils.btn("");
    this.skel = skel;
    
    // Configurable options
    this.preSubmit = config.preSubmit || function () {};
    this.successAction = config.successAction || function () {};
    this.failAction = config.failAction || function () {};
    this.url = config.url;
    this.title = config.title;
    this.noReset = config.noReset;
    this.populate(config.populate || {});
    this.toggleEdit(config.edit);

    this.$container.append(this.$messages, this.$form, this.$confirm);
};

// Put the form in or out of edit mode
utils.Form.prototype.toggleEdit = function (edit) {
    if (edit) {
        this.edit = true;
        this.submitAction = $.put;
        this.$confirm.attr("class", "btn btn-primary").text("Edit");
    } else {
        this.edit = false;
        this.submitAction = $.post;
        this.$confirm.attr("class", "btn btn-success").text("Submit");
    }
};

utils.Form.prototype._submit = function () {
    var self = this, json = {}, field;

    for (field in this.skel) {
        if (this.skel.hasOwnProperty(field)) {
            if (this.skel[field][0].type === "checkbox") {
                json[field] = this.skel[field][0].checked;
            } else {
                json[field] = this.skel[field].val();
            }
        }
    }

    this.preSubmit(json);

    this.submitAction(this.url, JSON.stringify(json), function (data) {
        var msg;

        if (data.success) {
            if (self.edit) {
                utils.info(
                    {msg: "Edit Successful.", dismiss: true},
                    self.$messages
                );
            } else {
                msg = "Submission Successful."
                if (data.id) {
                    msg += " Created with id " + data.id;
                }
                utils.success({msg: msg, dismiss: true}, self.$messages);
            }

            self.successAction(data, json);
            if (!self.edit && !self.noReset) {
                self.reset();
            }
        } else {
            self.failAction(data, json);
            self.$messages.empty();
            data.errors.forEach(function (e) {
                utils.info({msg: e, dismiss: true}, self.$messages);
            });
        }
    });
};

utils.Form.prototype.reset = function () {
    var field;

    for (field in this.skel) {
        if (this.skel.hasOwnProperty(field)) {
            this.skel[field].reset();
        }
    }
};

utils.Form.prototype.render = function () {
    var field, $elt, self = this;

    this.$confirm.unbind().click(this._submit.bind(this));
    this.$form.empty();
    if (this.title) {
        this.$form.append(utils.Form._wrap("", this.$title[0]));
    }
    for (field in this.skel) {
        if (this.skel.hasOwnProperty(field)) {
            $elt = this.skel[field];
            if ($elt.custom) {
                this.$form.append($elt);
            } else {
                this.$form.append(utils.Form._wrap($elt.label, $elt[0]));

                if ($elt[0].tagName.toLowerCase() === "input") {
                    $elt.unbind().keyup(function (e) {
                        if (e.which === 13) {
                            self.$confirm.click();
                        }
                    });
                }
            }
        }
    }
    return this.$container;
};

// Populate the form fields with values from the JSON object
utils.Form.prototype.populate = function (json) {
    var field;

    for (field in json) {
        if (json.hasOwnProperty(field) && this.skel.hasOwnProperty(field)) {
            this.skel[field].val(json[field]);
        }
    }
};

utils.Form._wrap = function (text, element) {
    var form = document.createElement("div"),
        label = document.createElement("label"),
        div = document.createElement("div");

    form.setAttribute("class", "form-group");
    label.setAttribute("class", "col-sm-2 control-label");
    div.setAttribute("class", "col-sm-10");

    label.innerHTML = text;

    form.appendChild(label);
    form.appendChild(div);
    div.appendChild(element);

    return form;
};

utils.Form._status = function (element, status) {
    var name1 = status, name2, span;

    if (status === "success") {
        name2 = "ok";
    } else if (status === "warning") {
        name2 = "warning-sign";
    } else {
        name2 = "remove";
    }

    span = document.createElement("span");
    span.className = "glyphicon glyphicon-" + name2 + " form-control-feedback";
    element.parentNode.parentNode.className =
        "form-group has-feedback has-" + name1;
    if (element.parentNode.getElementsByTagName("span").length) {
        element.parentNode.removeChild(
            element.parentNode.getElementsByTagName("span").item(0)
        );
    }
    element.parentNode.appendChild(span);
};
