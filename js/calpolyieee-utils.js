"use strict";

var utils = {};

utils.$messages = $("#messages");

utils.btn = function (text, color) {
    return $("<a href=\"javascript: void 0;\" class='btn btn-" +
             (color || "primary") + "'>").text(text);
};

utils.toMoney = function (cents) {
    if (cents >= 0) {
        return "$" + (cents / 100).toFixed(2);
    } else {
        return "($"  + (Math.abs(cents) / 100).toFixed(2) + ")";
    }
};

utils.rand = function (num) { return Math.floor(Math.random() * num); };

utils.date = function (s) { return (new Date(s * 1000)).toDateString(); };

utils.toTr = function () {
    return $("<tr>").append(
        Array.prototype.slice.call(arguments).map(function (thing) {
            return $("<td>").append(thing);
        })
    );
};

utils.arrayToTr = function (arr) {
    return $("<tr>").append(arr.map(function (thing) {
        return $("<td>").append(thing);
    }));
};

utils.orAlert = function (success, thisArg) {
    var self = this, $ctr = thisArg && thisArg.$messages || this.$messages;

    return function (data) {
        if (data.success) {
            success.call(thisArg || null, data)
        } else {
            $ctr.empty();
            data.errors.forEach(function (error) {
                self.info({msg: error, $ctr: $ctr});
            });
        }
    };
};

utils.doubleCheck = function (message, action) {
    var self = this, $imsure = this.btn("I'm sure", "danger");
    var $modalBody = $("<div>");

    if (!this._areYouSure) {
        this._areYouSure = new this.ModalAlert();
    }

    $modalBody.append("Are you sure you want to " + message);
    this.danger({
        msg: "This action cannot be undone!",
        $ctr: $modalBody
    });

    $imsure.click(function () {
        action();
        self._areYouSure.hide();
    });

    return function () {
        $("body").append(self._areYouSure.render({
            title: "Confirm Action",
            body: $modalBody,
            cancel: "No Way!",
            $confirm: $imsure
        }));

        self._areYouSure.show();
    };
};

utils.quickModal = function (title, $body) {
    var modal = new this.ModalAlert();

    $("body").append(modal.render({
        title: title,
        body: $body,
        cancel: "OK"
    }));
    modal.show();
};

utils._ajaxRequest = function (url, data, callback, type, method) {
    if ($.isFunction(data)) {
        callback = data;
        data = {};
    }

    return $.ajax({
        type: method,
        url: url,
        data: data,
        success: callback,
        dataType: type
    });
};

utils._alert = function (color, alert) {
    var $dismiss,
        $container = alert.$ctr || this.$messages,
        $frame = $("<div>").addClass("alert alert-" + color);

    $frame.html((typeof alert === "string") ? alert : alert.msg);

    if (!alert.noDismiss) {
        $dismiss = $("<button type=\"button\" data-dismiss=\"alert\">")
            .html("&times;")
            .addClass("close");
        $frame.addClass("alert-dismissable").append($dismiss);
    }

    if (alert.empty) {
        $container.empty();
    }

    $container.append($frame);
};

utils.success = utils._alert.bind(utils, "success");
utils.info    = utils._alert.bind(utils, "info");
utils.warning = utils._alert.bind(utils, "warning");
utils.danger  = utils._alert.bind(utils, "danger");
