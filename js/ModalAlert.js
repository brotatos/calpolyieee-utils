"use strict";

utils.ModalAlert = function () {
    this.$frame = $("<div class='modal fade weakHidden'>");
    this.$dialog = $("<div class='modal-dialog'>");
    this.$content = $("<div class='modal-content'>");
    this.$header = $("<div class='modal-header'>")
        .append($("<button type='button' class='close' data-dismiss='modal'>").html("&times;"));

    this.$title = $("<h4 class='modal-title'>");
    this.$body = $("<div class='modal-body'>");
    this.$footer = $("<div class='modal-footer'>");
    this.$cancel = utils.btn("", "default").attr("data-dismiss", "modal");

    this.$header.append(this.$title);
    this.$footer.append(this.$cancel);
    this.$content.append(this.$header, this.$body, this.$footer);
    this.$frame.append(this.$dialog.append(this.$content));
};

utils.ModalAlert.prototype.render = function (alert) {
    this.$title.text(alert.title);
    this.$body.empty().append(alert.body);
    this.$cancel.text(alert.cancel).unbind();
    if (alert.$confirm) {
        alert.$confirm.click(function () { alert.$confirm.detach(); });
        this.$footer.append(alert.$confirm);
        this.$cancel.click(function () { alert.$confirm.detach(); });
    }

    return this.$frame;
};

utils.ModalAlert.prototype.show = function () { this.$frame.modal("show"); };
utils.ModalAlert.prototype.hide = function () { this.$frame.modal("hide"); };
