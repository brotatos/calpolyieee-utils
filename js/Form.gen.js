"use strict";

utils.Form.gen = {};

utils.Form.gen.textarea = function (label, rows) {
    var $in = $("<textarea class='form-control' rows=" + (rows || 3) + ">");

    $in.label = label;
    $in.reset = function () { $in.val(""); };
    return $in;
};

utils.Form.gen.p = function (label, html) {
    var $in = $("<p class='form-control-static'>").html(html);

    $in.label = label;
    $in.reset = function () {};
    return $in;
};

utils.Form.gen.input = function (label, placeholder) {
    var $in = $("<input class=\"form-control\" placeholder='" +
                (placeholder || "") + "'type=\"text\">");

    $in.label = label;
    $in.reset = function () { $in.val(""); };
    return $in;
};

utils.Form.gen.password = function (label, placeholder) {
    var $in = $("<input class=\"form-control\" placeholder='" +
                (placeholder || "") + "'type=\"password\">");

    $in.label = label;
    $in.reset = function () { $in.val(""); };
    return $in;
};

utils.Form.gen.money = function (label) {
    var $in = $("<input class=\"form-control\" type=\"text\">");
    var $group = $("<div class=\"input-group money\">");
    var addon = document.createElement("div");

    addon.className = "input-group-addon";
    addon.innerHTML = "$";
    $group.append(addon, $in);

    $group.label = label;
    $group.val = function (cents) {
        if (cents) {
            $in.val((cents / 100).toFixed(2));
        } else {
            return parseInt(parseFloat($in.val() * 100), 10);
        }
        return $group;
    };
    $group.reset = function () { $in.val(""); };
    return $group;
};

utils.Form.gen.date = function (label) {
    var $in = $("<input class=\"form-control\" type=\"date\">");

    $in.label = label;
    $in._val = $in.val;
    $in.val = function (seconds) {
        if (seconds) {
            $in._val(new Date(parseInt(seconds, 10) * 1000).toJSON()
                     .substr(0, 10));
        } else {
            return parseInt(new Date($in._val()).getTime() / 1000, 10);
        }
        return $in;
    };
    $in.reset = function () { $in.val(Date.now() / 1000); };
    return $in;
};

utils.Form.gen.checkbox = function (label) {
    var $in = $("<input type='checkbox'>");

    $in.label = label;
    $in.reset = function () { $in[0].checked = false; };
    return $in;
};

utils.Form.gen.select = function (label) {
    var $in = $("<select class='form-control'>");

    $in.label = label;
    $in.fill = function (array, choose) {
        if (array.length && typeof array[0] === "string") {
            array = array.map(function (elt) {
                return {text: elt, val: elt};
            });
        }

        $in.append(
            choose && "<option>Choose...</option>" || "",
            array.map(function (elt) {
                return "<option value=\"" + elt.val + "\">" + elt.text + "</option>";
            })
        );
    };
    $in.reset = function () { $in.val("Choose..."); };
    return $in;
};

utils.Form.gen.custom = function ($in, val, reset) {
    $in._val = $in.val;
    
    return $.extend($in, {
        val: val,
        reset: reset,
        custom: true
    });
};
